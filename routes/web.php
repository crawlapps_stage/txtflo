<?php

use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Setting\SettingController;
use App\Http\Controllers\Test\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('layouts.app');
})->middleware(['auth.shopify'])->name('home');

Route::group(['middleware' => ['auth.shopify']], function () {
    Route::resource('dashboard', DashboardController::class);
    Route::get('webhook', [DashboardController::class, 'webhookIndex']);
    Route::get('status', [DashboardController::class, 'changeStatus']);
    Route::post('crontime', [DashboardController::class, 'crontime'])->name('crontime');
    Route::resource('setting', SettingController::class);
});

Route::get('flush', function(){
    request()->session()->flush();
});

Route::get('/test', [TestController::class, 'index1'])->name('test');
