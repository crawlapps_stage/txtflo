<?php

namespace App\Traits;

use App\Models\AuthToken;
use App\Models\Event;
use App\Models\Setting;
use App\Models\User;
use App\Models\Webhook;
use Illuminate\Support\Facades\Auth;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;

/**
 * Trait TxtFlo
 * @package App\Traits
 */
trait TxtFlo
{
    /**
     * @return mixed
     */
    public function index($domain, $topic, $order_id, $type, $data = [] ){
        logger("=============== $topic TRAIT =============");
        try{
            $shop = User::where('name', $domain)->first();
            $token = AuthToken::where('user_id', $shop->id)->first();
            $webhook = Webhook::where('user_id', $shop->id)->where('topic', $topic)->where('status', 1)->first();
            if( $token && $webhook){
                if( $type == 'abandoned' ){
//                    logger(json_encode($data));
                    $order = $data;
                }else{
                    //                $parameter['fields'] = 'id,line_items,order_number,customer';
                    $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/orders/'.$order_id.'.json';
                    $sh_order = $shop->api()->rest('GET', $endPoint);
                    $order = $sh_order['body']->container['order'];
                }

                $phone = '';
                if( $order['phone'] != '' ){
                    $phone = $order['phone'];
                }elseif ( @$order['customer']['phone'] && $order['customer']['phone'] ){
                    $phone = $order['customer']['phone'];
                }elseif ( @$order['customer']['default_address']['phone'] && $order['customer']['default_address']['phone'] ){
                    $phone = $order['customer']['default_address']['phone'];
                }
//                $phone = ( $phone == '' ) ? '+91 9635569856' : $phone;
                if( $phone != ''){
                    $contact = $this->getContact($order['customer'], $phone);
                    $custom_map = $this->customMap($order, $shop);
                    $tags = $this->tags($order, $shop->id, $type);
                    $country = ( @$order['customer']['default_address'] ) ? $order['customer']['default_address']['country_code'] : '';
                    $result = $this->addKeyToTxtflo($custom_map, $token->token, $contact, $tags, $country);

                    $event = new Event;
                    $event->user_id = $shop->id;
                    $event->resource_id = $order_id;
                    $event->event = $topic;
                    $event->response = $result;
                    $event->save();
                }
            }
        }catch( \Exception $e ){
           logger($e);
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function productVariants($id)
    {
        try {
            return '{
                         productVariant(id: "gid://shopify/ProductVariant/'.$id.'") {
                            selectedOptions {
                                  name
                                  value
                            }
                         }
                    }';
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @param $query
     * @return array|\GuzzleHttp\Promise\Promise
     * @throws \Exception
     */
    public function request($query, $shop)
    {
        $parameter = [];
        $options = new Options();
        $options->setVersion('2020-07');
        $api = new BasicShopifyAPI($options);
        $api->setSession(new Session(
            $shop->name, $shop->password));
        return $api->graph($query, $parameter);
    }

    /**
     * @param $custom_map
     * @param $token
     * @param $contact
     */
    public function addKeyToTxtflo($custom_map, $token, $contact, $tags, $country)
    {
        $data = [
            'api_token' => $token,
            'fallback_company_id' => 1,
            'country' => 'IN',
            'contact' => $contact,
            'custom_map_operator' => 'OR',
            'custom_map' => $custom_map,
            'tags' => $tags,
        ];
        logger(json_encode($data));
        $c  = curl_init();
        curl_setopt (  $c ,  CURLOPT_FRESH_CONNECT , TRUE );
        curl_setopt (  $c ,  CURLOPT_URL  ,  "https://txtflo.com/api/token/contact_to_key" );
        curl_setopt (  $c , CURLOPT_POST ,
            true );
        curl_setopt (  $c ,  CURLOPT_POSTFIELDS  ,  http_build_query (  $data ));
        curl_setopt (  $c ,  CURLOPT_TIMEOUT  ,  10 );
        curl_setopt (  $c ,  CURLOPT_RETURNTRANSFER ,
            true );
        $response  =  curl_exec ( $c );
        logger($response);
        return $response;
    }

    /**
     * @param $customer
     * @return array
     */
    public function getContact($customer, $phone)
    {
        $contact = [];
        if( !empty($customer) ){
            $contact['mobile_phone'] = $phone ;
            $contact['first_name'] = $customer['first_name'];
            $contact['last_name'] = $customer['last_name'];
            $contact['email'] = $customer['email'];
        }
        return $contact;
    }

    /**
     * @param $lineitems
     * @return array
     * @throws \Exception
     */
    public function customMap($order, $shop){
        $setting = Setting::where('key', 'custom_map')->where('user_id', $shop->id)->first();
        $cmvalue = json_decode($setting->value);
        $custom_map = [];
        $arr = [ 'billing_address', 'shipping_address', 'client_details', 'payment_details'];
        if( !empty($cmvalue) ) {
            foreach ($cmvalue as $key => $val) {
                if (in_array($val->option, $arr)) {
                    $data = $order[$val->option];
                    if( !empty( $data ) ){
                        foreach ( $data as $k=>$v ){
                            $key = ( $val->is_checked ) ? $val->option .'_'.$val->key .'_'. $k : $val->key.'_'. $k;
                            $cm['key'] = $key;
                            $cm['value'] =  $v;
                            array_push($custom_map, $cm);
                        }
                    }
                } elseif ($val->option == 'selected_options') {
                    $lineitems = $order['line_items'];
                    if( !empty($lineitems) ){
                        foreach ( $lineitems as $lkey=>$lval ){
                            $query = $this->productVariants($lval['variant_id']);
                            $sh_variant = $this->request($query, $shop);
                            if( !$sh_variant['errors'] ){
                                $option_data = $sh_variant['body']->container['data'];
                                $sh_options = (@$option_data['productVariant']['selectedOptions']) ? @$option_data['productVariant']['selectedOptions'] : [];
                                if( !empty($sh_options) ){
                                    foreach ( $sh_options  as $okey=>$oval){
                                        $key = ( $val->is_checked ) ? $val->key .'_'. $oval['name'] : $val->key .'_'.$oval['name'];
                                        $cm['key'] = $key;
                                        $cm['value'] = $oval['value'];
                                        array_push($custom_map, $cm);
                                    }
                                }
                            }
                        }
                    }
                }else {
                    $key = ( $val->is_checked ) ? $val->key . '_' . $val->option : $val->key;
                    $cm['key'] = $key;
                    $cm['value'] =  $order[$val->option];
                    array_push($custom_map, $cm);
                }
            }
        }
        return $custom_map;
    }

//    public function customMap($lineitems, $shop){
//        $custom_map = [];
//        if( !empty($lineitems) ){
//            foreach ( $lineitems as $key=>$val ){
//                $query = $this->productVariants($val['variant_id']);
//                $sh_variant = $this->request($query, $shop);
//                if( !$sh_variant['errors'] ){
//                    $option_data = $sh_variant['body']->container['data'];
//                    $sh_options = (@$option_data['productVariant']['selectedOptions']) ? @$option_data['productVariant']['selectedOptions'] : [];
//                    if( !empty($sh_options) ){
//                        foreach ( $sh_options  as $okey=>$oval){
//                            $cm['key'] = $oval['name'];
//                            $cm['value'] = $oval['value'];
//                            array_push($custom_map, $cm);
//                        }
//                    }
//                }
//            }
//        }
//
//        return $custom_map;
//    }
    public function tags($order, $userId, $type){
        $setting = Setting::where('key', 'tags')->where('user_id', $userId)->first();
        $tagvalue = json_decode($setting->value);
        $tags = [];
        foreach ( $tagvalue as $key=>$val ){
            if( ($type == 'abandoned' && $key == 'order_number') || ($type == 'abandoned' && $key == 'contact_email') ){

            }else{
                if( $val ){
                    $cm['key'] = $key;
                    $cm['value'] = $order[$key];
                    array_push($tags, $cm);
                }
            }
        }
        return $tags;
    }
}
