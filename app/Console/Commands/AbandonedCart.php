<?php

namespace App\Console\Commands;

use App\Models\Event;
use App\Models\User;
use App\Traits\TxtFlo;
use Illuminate\Console\Command;

class AbandonedCart extends Command
{
    use TxtFlo;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'abandoned:cart {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Abandoned cart job';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        logger(date('Y-m-d H:i:s'));
        logger($this->argument('user_id'));
        logger("-------------- Abandoned Cart --------------");
        try{
            $userid = $this->argument('user_id');
//            $users = User::all();
//            if( count( $users ) > 0 ){
//                foreach ( $users as $key=>$val ){
                    $user = User::where('id', $userid)->first();
                    logger( $user->name );
                    $endPoint = '/admin/api/'.env('SHOPIFY_API_VERSION').'/checkouts.json';
                    $result = $user->api()->rest('GET', $endPoint);
                    if( !$result['errors'] ){
                        $sh_orders = $result['body']['checkouts'];
                        foreach ( $sh_orders as $orderkey=>$orderval ){
                            $is_added = Event::where('user_id', $userid)->where('resource_id', $orderval['id'])->first();

                            ( !$is_added ) ? $this->index($user->name, 'abandonedcart', $orderval['id'], 'abandoned', $orderval) : '';
                        }
                    }
//                }
//            }
        }catch( \Exception $e ){
            logger(json_encode($e));
        }
    }
}
