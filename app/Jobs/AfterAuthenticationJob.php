<?php

namespace App\Jobs;

use App\Models\Setting;
use App\Models\Webhook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('-----------------------AFTER AUTHENTICATION JOB-----------------------');
        try {
            $shop = \Auth::user();
            $topics = ['orders/create' => 'Order Create', 'abandonedcart' => 'Abandoned Cart'];

            foreach ( $topics as $key=>$val ){
                $entity = Webhook::where('user_id', $shop->id)->where('topic', $key)->first();
                if (!$entity) {
                    $entity = new Webhook;
                    $entity->user_id = $shop->id;
                    $entity->topic = $key;
                    $entity->display_text = $val;
                    $entity->status = 0;
                    $entity->save();
                }
            }

            $tags = ['email'=> false, 'name' => false, 'total_price'=> false, 'currency'=> false, 'order_number'=> false, 'contact_email'=> false ];
            $setting = Setting::where('key', 'tags')->where('user_id', $shop->id)->first();
            if( !$setting ){
                $setting = new Setting;
                $setting->user_id = $shop->id;
                $setting->key = 'tags';
                $setting->value = json_encode($tags);
                $setting->save();
            }
            $setting = Setting::where('key', 'custom_map')->where('user_id', $shop->id)->first();
            if( !$setting ){
                $arr = [['key' => '', 'option' => 'name', 'is_checked' => false]];
                $setting = new Setting;
                $setting->user_id = $shop->id;
                $setting->key = 'custom_map';
                $setting->value = json_encode($arr);
                $setting->save();
            }

        }catch (\Exception $e) {
            logger('-----------------------ERROR :: AFTER AUTHENTICATION JOB-----------------------');
            logger($e);
        }
    }
}
