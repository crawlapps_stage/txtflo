<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\ValidationException;

class SettingRequest extends FormRequest
{
    public static $rules = [];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = Self::$rules;
        $data = $this::all();
        switch (Route::currentRouteName()) {
            case 'setting.store':
            {
                $cm = $data['data']['custom_map'];
                foreach ( $cm as $key=>$val ){
                    if( !$val['is_checked'] ){
                        $rules['data.custom_map.' . $key . '.key'] = 'required';
                    }
                }

                return $rules;
            }
            default:
                break;
        }
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        $data = $this::all();
        $cm = $data['data']['custom_map'];
        $rules = [];
        foreach ( $cm as $key=>$val ){
            $rules['data.custom_map.' . $key . '.key'] = 'required';
        }
        return $rules;
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson()) {
            $response = new JsonResponse($validator->errors(), 422);
            throw new ValidationException($validator, $response);
        }

        throw (new ValidationException($validator))
            ->errorBag($this->errorBag);
    }
}
