<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\WebhookRequest;
use App\Models\AuthToken;
use App\Models\User;
use App\Models\Webhook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Response;

class DashboardController extends Controller
{
    /**
     * @return mixed
     */
    public function index(){
        try{
            $shop = Auth::user();
            $istoken = AuthToken::select('id', 'token')->where('user_id', $shop->id)->first();

            $token = ($istoken) ? $istoken->token : array('id' => '', 'token' => '');
            return response::json(['data' => $token], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return mixed
     */
    public function store( Request $request){
        try{
            $data = $request->data;
            $shop = Auth::user();
            $istoken = AuthToken::where('user_id', $shop->id)->first();
            $token = ($istoken) ? $istoken : new AuthToken;
            $token->user_id = $shop->id;
            $token->token = $data['token'];
            $token->save();

            return response::json(['data' => 'Saved!!'], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @return mixed
     */
    public function webhookIndex(){
        try{
            $shop = Auth::user();
            $webook = Webhook::where('user_id', $shop->id)->get();
            $res['webhook'] = $webook;
            $res['cron']['user_id'] = $shop->id;
            $res['cron']['crontime'] = $shop->crontime;
            return response::json(['data' => $res], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return mixed
     */
    public function changeStatus(Request $request){
        try{
            $shop = Auth::user();
            $webook = Webhook::where('id', $request->id)->first();
            $webook->status = !$webook->status;
            $webook->save();
            return response::json(['data' => $webook], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return mixed
     */
    public function crontime(WebhookRequest $request){
        try{
            $data = $request->data;

            $user = User::where('id', $data['user_id'])->first();
            $user->crontime = $data['crontime'];
            $user->save();
            return response::json(['data' => 'Saved!'], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }
}
