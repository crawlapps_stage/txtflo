<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Models\AuthToken;
use App\Traits\TxtFlo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Osiset\BasicShopifyAPI\BasicShopifyAPI;
use Osiset\BasicShopifyAPI\Options;
use Osiset\BasicShopifyAPI\Session;

class TestController extends Controller
{
    use TxtFlo;

    public function index1(){
        $shop = Auth::user();
        $this->index($shop->name, 'orders/create', '2730475651232', '', []);
    }

}
