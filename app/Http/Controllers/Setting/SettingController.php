<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Models\Setting;
use Illuminate\Http\Request;
use Response;

class SettingController extends Controller
{
    /**
     * @param  Request  $request
     * @return mixed
     */
    public function index( Request $request){
        try{
            $shop = \Auth::user();
            $tagsSetting = Setting::select('value')->where('key', 'tags')->where('user_id', $shop->id)->first();
            $cmSetting = Setting::select('value')->where('key', 'custom_map')->where('user_id', $shop->id)->first();

            $data['tags'] = json_decode($tagsSetting->value);
            $data['custom_map'] = json_decode($cmSetting->value);
            return response::json(['data' => $data], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return mixed
     */
    public function store( SettingRequest $request){
        try{
            $shop = \Auth::user();
            $data = $request->data;
//            save tags
            $setting = Setting::where('key', 'tags')->where('user_id', $shop->id)->first();
            $setting->value = json_encode($data['tags']);
            $setting->save();

//            save custom maps
            $setting = Setting::where('key', 'custom_map')->where('user_id', $shop->id)->first();
            $setting->value = json_encode($data['custom_map']);
            $setting->save();
            return response::json(['data' => 'Saved!!'], 200);
        }catch( \Exception $e ){
            return response::json(['data' => $e->getMessage()], 422);
        }
    }
}
